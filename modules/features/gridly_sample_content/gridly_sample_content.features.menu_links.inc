<?php
/**
 * @file
 * gridly_sample_content.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function gridly_sample_content_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_design:category/design
  $menu_links['main-menu_design:category/design'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'category/design',
    'router_path' => 'category/%',
    'link_title' => 'Design',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_design:category/design',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_home:<front>
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_photography:category/photography
  $menu_links['main-menu_photography:category/photography'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'category/photography',
    'router_path' => 'category/%',
    'link_title' => 'Photography',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_photography:category/photography',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_styles:category/web-design
  $menu_links['main-menu_styles:category/web-design'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'category/web-design',
    'router_path' => 'category/%',
    'link_title' => 'Styles',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_styles:category/web-design',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Design');
  t('Home');
  t('Photography');
  t('Styles');


  return $menu_links;
}
