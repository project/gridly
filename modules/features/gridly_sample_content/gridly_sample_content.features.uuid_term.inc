<?php
/**
 * @file
 * gridly_sample_content.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function gridly_sample_content_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Web Design',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '1ecca6e5-ac67-4d96-9325-095408cd5190',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'Design',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '47a19689-57a5-4d6d-8077-3ea61afe12e4',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'Print Design',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'b6a5836a-7263-409d-9ebb-670cd1c563be',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'Graphic Design',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'e2882a7c-bcaa-440b-b74b-acde0a752bb8',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'Photography',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'fbc61a55-c132-4573-8e6c-63db7579c55a',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'Styles',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'fecf616a-313d-46b1-9bb9-d4ae85174b2c',
    'vocabulary_machine_name' => 'tags',
  );
  return $terms;
}
