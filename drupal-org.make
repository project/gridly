; gridly make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[admin_menu][version] = "3.0-rc5"
projects[admin_menu][subdir] = "contrib"

projects[module_filter][version] = "2.1"
projects[module_filter][subdir] = "contrib"

projects[ctools][version] = "1.14"
projects[ctools][subdir] = "contrib"

projects[features][version] = "2.10"
projects[features][subdir] = "contrib"

projects[uuid_features][version] = "1.0-rc1"
projects[uuid_features][subdir] = "contrib"

projects[masonry][version] = "2.0"
projects[masonry][subdir] = "contrib"

projects[masonry_views][version] = "3.0"
projects[masonry_views][subdir] = "contrib"

projects[node_export][version] = "3.1"
projects[node_export][subdir] = "contrib"

projects[entity][version] = "1.9"
projects[entity][subdir] = "contrib"

projects[libraries][version] = "2.3"
projects[libraries][subdir] = "contrib"

projects[prev_next][version] = "2.x-dev"
projects[prev_next][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[uuid][version] = "1.2"
projects[uuid][subdir] = "contrib"

projects[jquery_update][version] = "2.7"
projects[jquery_update][subdir] = "contrib"

projects[responsive_menus][version] = "1.6"
projects[responsive_menus][subdir] = "contrib"

projects[wysiwyg][version] = "2.5"
projects[wysiwyg][subdir] = "contrib"

projects[views][version] = "3.20"
projects[views][subdir] = "contrib"

; +++++ Themes +++++

; omega
projects[omega][type] = "theme"
projects[omega][version] = "4.2"
projects[omega][subdir] = "contrib"

; +++++ Libraries +++++

; jQuery Masonry
libraries[masonry][directory_name] = "masonry"
libraries[masonry][type] = "library"
libraries[masonry][destination] = "libraries"
libraries[masonry][download][type] = "get"
libraries[masonry][download][url] = "https://raw.githubusercontent.com/desandro/masonry/v2-gh-pages/jquery.masonry.min.js"

; CKEditor
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"
libraries[ckeditor][destination] = "libraries"
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.tar.gz"
