<?php

/**
 * Implements hook_preprocess_page().
 */
function gridly_preprocess_page(&$vars) {
  $vars['has_featured'] = FALSE;
  $vars['featured_image'] = array();
  $vars['tags'] = array();
  if (!isset($vars['node'])) {
    return;
  }
  $node = $vars['node'];
  $lang = $node->language;
  if (isset($node) && isset($node->field_featured_image) && isset($node->field_featured_image[$lang][0])) {
    $vars['has_featured'] = TRUE;
    $vars['featured_image'] = field_view_field('node', $node, 'field_featured_image', array(
    'label' => 'hidden',
    'settings' => array(
      'image_style' => 'featured_image_style',
    ),
    ));
    $vars['tags'] = field_view_field('node', $node, 'field_tags', array(
    'label' => 'hidden'));
  }
}
