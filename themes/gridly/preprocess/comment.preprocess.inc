<?php

/**
 * @file
 * Contains a pre-process hook for 'comment'.
 */

/**
 * Implements hook_preprocess_comment().
 */
function gridly_preprocess_comment(&$vars) {
  $vars['submitted'] = t('!username says:', array('!username' => $vars['author']));
}
