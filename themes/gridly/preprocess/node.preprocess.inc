<?php

/**
 * @file
 * Contains a pre-process hook for 'node'.
 */

/**
 * Implements hook_preprocess_node().
 * Override submitted info format.
 */
function gridly_preprocess_node(&$vars) {
  $vars['submitted'] = t('@date', array('@date' => date("F j, Y", $vars['created'])));
}
